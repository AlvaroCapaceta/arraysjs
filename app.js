// Arreglo de 20 valores
const array = [
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
];

// Sacar el promedio de los elementos del arreglo
const promedio = (array) => {
  let suma = 0;

  for (let i = 0; i < array.length; i++) {
    suma += array[i];
  }

  const promedio = suma / array.length;
  return promedio;
};

console.log("Promedio:", promedio(array));

// Sacar la cantidad de numeros par que hay en un arreglo
const pares = (array) => {
  let pares = 0;

  for (let i = 0; i < array.length; i++) {
    if (array[i] % 2 == 0) pares++;
  }

  return pares;
};

console.log("Numeros pares:", pares(array));

// Imprimir el array
const imprimir = (array) => {
  console.log(array.join());
};

// Ordenar numeros en un array de mayor a menor
const ordenar = (array) => {
  const comparar = (a, b) => b - a;
  array.sort(comparar);
  return imprimir(array);
};

console.log("Array original:");
imprimir(array);

console.log("Orden de mayor a menor:");
ordenar(array);
